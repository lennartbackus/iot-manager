import React from 'react';
import { Formik, Form, Field } from 'formik';
import { Button, LinearProgress } from '@material-ui/core';
import { TextField } from 'formik-material-ui';
import { useMutation } from '@apollo/client';
import { LOGIN_MUTATION } from '../../_services/graphql/mutations/login';

interface Values {
    username: string;
    password: string;
}

export default function LoginForm(): JSX.Element {
	const [login] = useMutation(LOGIN_MUTATION);
	return (
		<Formik
			initialValues={{
				username: '',
				password: '',
			}}
			validate={values => {
				const errors: Partial<Values> = {};
				if (!values.username) {
					errors.username = 'Required';
				}
				if (!values.password) {
					errors.password = 'Required';
				}
				return errors;
			}}
			onSubmit={(values) => {
				login({ variables: { username: values.username, password: values.password } });
			}}
		>
			{({ submitForm, isSubmitting }) => (
				<Form>
					<Field
						component={TextField}
						name="username"
						type="username"
						label="Username"
					/>
					<br />
					<Field
						component={TextField}
						name="password"
						type="password"
						label="Password"
					/>
					{isSubmitting && <LinearProgress />}
					<br />
					<br />
					<Button
						variant="contained"
						color="primary"
						disabled={isSubmitting}
						onClick={submitForm}
					>
            Submit
					</Button>
				</Form>                         
			)}
		</Formik>
	);
}