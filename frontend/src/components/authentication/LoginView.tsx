import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import LockIcon from '@material-ui/icons/Lock';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';
import LoginForm from './LoginForm';

const useStyles = makeStyles(() =>
	createStyles({
		root: {
			minWidth: '40%',
			minHeight: '20vh',
			display: 'flex',
			marginTop: '-100px',
		},
		title: {
			flexGrow: 1,
		},
	}),
);

export default function LoginView(): JSX.Element {
	const classes = useStyles();
	return (
		<Card className={classes.root}>
			<CardContent>
				<Typography variant="h5" className={classes.title}>
					<LockIcon></LockIcon>Login
				</Typography>
				<LoginForm></LoginForm>
			</CardContent>
		</Card>
	);
}