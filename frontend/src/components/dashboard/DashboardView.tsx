import React from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';


const useStyles = makeStyles(() =>
	createStyles({
		root: {
			minWidth: '40%',
			minHeight: '20vh',
			display: 'flex',
			marginTop: '-100px',
		},
		title: {
			flexGrow: 1,
		},
	}),
);

export default function DashboardView(): JSX.Element {
	const classes = useStyles();
	return (
		<Card className={classes.root}>
			<CardContent>
				Dashboard
			</CardContent>
		</Card>
	);
}