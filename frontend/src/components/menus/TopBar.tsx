import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { useQuery, useMutation } from '@apollo/client';
import { SESSION_QUERY } from '../../_services/graphql/queries/session';
import { LOGOUT_MUTATION } from '../../_services/graphql/mutations/logout';

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			height: '70px',
      position: 'relative',
      zIndex: theme.zIndex.drawer + 1,
		},
		menuButton: {
			marginRight: theme.spacing(2),
		},
		title: {
			flexGrow: 1,
		},
	}),
);

export default function TopBar (): JSX.Element {
	const classes = useStyles();
	const [logout] = useMutation(LOGOUT_MUTATION);
	const session = useQuery(SESSION_QUERY);
	return (
		<AppBar className={classes.root}>
			<Toolbar>
				<Typography variant="h6" className={classes.title}>
          IoT Manager
				</Typography>
				{session.data.session.isLoggedIn && <Button color="inherit" onClick={() => logout()}>Logout</Button>}
			</Toolbar>
		</AppBar>
	);
}