import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import Drawer from '@material-ui/core/Drawer';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DashboardIcon from '@material-ui/icons/Dashboard';
import SettingsIcon from '@material-ui/icons/Settings';
import SettingsInputAntennaIcon from '@material-ui/icons/SettingsInputAntenna';

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
    },
    drawerContainer: {
      overflow: 'auto',
    },
  }),
);

export default function SideMenu() {
  const classes = useStyles();

  return (
    <Drawer
        className={classes.root}
        variant="permanent"
        classes={{
            paper: classes.drawerPaper,
        }}
        >
        <Toolbar />
        <div className={classes.drawerContainer}>
            <List>
                <ListItem button key="dashboard" component={Link} to={'/'}>
                <ListItemIcon><DashboardIcon /></ListItemIcon>
                <ListItemText primary="Dashboard" />
                </ListItem>
                <ListItem button key="devices" component={Link} to={'/devices'}>
                <ListItemIcon><SettingsInputAntennaIcon /></ListItemIcon>
                <ListItemText primary="Devices" />
                </ListItem>
            </List>
            <Divider />
            <List>
            <ListItem button key="settings" component={Link} to={'/settings'}>
                <ListItemIcon><SettingsIcon /></ListItemIcon>
                <ListItemText primary="Settings" />
                </ListItem>
            </List>
        </div>
    </Drawer>
  );
}
