import React from 'react';
import { Route } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import TopBar from './components/menus/TopBar';
import SideMenu from './components/menus/SideMenu';
import LoginView from './components/authentication/LoginView';
import DashboardView from './components/dashboard/DashboardView';
import SettingsView from './components/settings/SettingsView';
import DevicesView from './components/devices/DevicesView';
import { useQuery } from '@apollo/client';
import Grid from '@material-ui/core/Grid';
import { SESSION_QUERY } from './_services/graphql/queries/session';

const useStyles = makeStyles({
	root: {
		background: '#000000',
		height: '100vh',
		width: '100%',
	},
	content: {
		background: '#DDDDDD',
		height: '100vh',
		width: '100%',
		position: 'fixed',
		alignItems: 'center',
		justifyContent: 'center',
	},
	main: {
	},
});

export default function App(): JSX.Element {

	const classes = useStyles();

	const session = useQuery(SESSION_QUERY);
  
	if (session.loading) 
		return (<h1>Loading...</h1>);

	return (
		<div className={classes.root}>
			<TopBar></TopBar>
      {session.data.session.isLoggedIn && <SideMenu></SideMenu>}
			<Grid container className={classes.content}>
				<Grid item>
					{!session.data.session.isLoggedIn
						?<LoginView></LoginView>
						:(
							<main className={classes.main}>
								<Route path="/" exact component={ DashboardView } />
								<Route path="/devices" exact component={ DevicesView } />
								<Route path="/settings" exact component={ SettingsView } />
							</main>
						)
					}
				</Grid>
			</Grid>
		</div>
	);
}