import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client';
import { onError } from '@apollo/client/link/error';

const httpLink = createHttpLink({
	uri: '/api/graphql',
});

const errorLink = onError(({ graphQLErrors, networkError }) => {
	if (graphQLErrors)
		graphQLErrors.map(({ message, locations, path }) =>
			console.log(
				`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
			),
		);

	if (networkError) 
		console.log(
			`[Netowrk error]: Message: ${networkError}`,
		);
});

export const client = new ApolloClient({
	link: errorLink.concat(httpLink),
	cache: new InMemoryCache({
		typePolicies: {
			Session: {
				keyFields: [],
			}
		}
	})
});
