
import { gql } from '@apollo/client';

export const SESSION_QUERY = gql`
  query Session { 
    session {
      isLoggedIn
      message
    } 
  }
`;
