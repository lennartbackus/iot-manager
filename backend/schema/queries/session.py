"""
session query to check django session.
"""
from backend.schema.models.session import Session
from backend.schema.root_query import root_query


@root_query('session', Session)
def session_resolver(root, info, **kwargs):
    """ Resolve function for 'session' query field """

    if info.context.user.is_authenticated:
        return {'isLoggedIn': True}

    return {'isLoggedIn': False}
