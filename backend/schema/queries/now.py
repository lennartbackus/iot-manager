""" Exaple root query entrypoint { now }

Demonstration of how to apply the

   @root_quey(name, field_type)

decorator
"""
from datetime import datetime

import graphene

from backend.schema.root_query import root_query


@root_query('now', graphene.DateTime)
def now_resolver(root, info, **kwargs):
    """ Resolve function for 'now' query field """
    return datetime.now()
