"""
RootQuery is the template for all GraphQL queries.
Queries are added using the @root_query decorator
in files in the queries folder.
"""
import graphene


class RootQuery(graphene.ObjectType):
    """ RootQuery, entry point for all queries. """


def root_query(name, field_type):
    """
    Decorator for RootQuery modules
    Functions using this decorator are autmatcally added to
    the RootQuery class at runtime.
    """
    def wrap(f):
        RootQuery._meta.fields.update({name: graphene.Field(field_type)})
        setattr(RootQuery, name, field_type)
        setattr(RootQuery, f'resolve_{name}', f)
        return f
    return wrap
