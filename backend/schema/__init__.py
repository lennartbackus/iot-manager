""" Graphene schema that is exposed to the API """
import graphene

from .mutations import *  # noqa
from .queries import *  # noqa
from .root_mutation import RootMutation
from .root_query import RootQuery
from .root_subscription import RootSubscription
from .subscriptions import *  # noqa

schema = graphene.Schema(
    query=RootQuery,
    mutation=RootMutation,
    subscription=RootSubscription,
)
