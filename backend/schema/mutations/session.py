from django.contrib.auth import authenticate
from django.contrib.auth import login
from django.contrib.auth import logout
from graphene import Boolean
from graphene import Field
from graphene import InputObjectType
from graphene import Mutation
from graphene import String

from backend.schema.models.session import Session
from backend.schema.root_mutation import root_mutation


class LoginInput(InputObjectType):
    username = String(required=True)
    password = String(required=True)


@root_mutation('login')
class Login(Mutation):
    class Arguments:
        input = LoginInput()

    ok = Boolean()
    session = Field(lambda: Session)

    def mutate(root, info, input):
        user = authenticate(
            username=input.username,
            password=input.password,
        )
        if user is None:
            return Login(
                ok=False,
                session={
                    'isLoggedIn': False,
                    'message': 'Invalid credentials.',
                },
            )
        login(info.context, user)
        return Login(
            ok=True,
            session={
                'isLoggedIn': True,
                'message': 'You are now logged in.',
            },
        )


@root_mutation('logout')
class Logout(Mutation):

    ok = Boolean()
    session = Field(lambda: Session)

    def mutate(root, info):
        logout(info.context)
        return Logout(
            ok=True,
            session={
                'isLoggedIn': False,
                'message': 'You are now logged out.',
            },
        )
