"""
Session GraphQL model

Represents the Django login state
"""
from graphene import Boolean
from graphene import ObjectType
from graphene import String


class Session(ObjectType):
    isLoggedIn = Boolean()
    message = String()
