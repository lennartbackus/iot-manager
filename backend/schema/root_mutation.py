"""
RootMutation is the template for all GraphQL mutations.
Mutations are added using the @root_mutation decorator
in files in the mutations folder.
"""
import graphene


class RootMutation(graphene.ObjectType):
    """ RootMutation, entry point for all mutations """


def root_mutation(name):
    """
    Decorator for RootMutation modules
    Mutation classes using this decorator are autmatcally added to
    the RootMutation class at runtime.
    """
    def wrap(c):
        RootMutation._meta.fields.update({name: c.Field()})
        setattr(RootMutation, name, c.Field())
        return c
    return wrap
