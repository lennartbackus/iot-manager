"""
RootSubscription
"""
import graphene


class RootSubscription(graphene.ObjectType):
    """ RootSubscription, entry point for all subscriptions. """
    ok = graphene.Boolean()

    def resolve_ok(root, info, **kwargs):
        return True
